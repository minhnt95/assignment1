package com.higg;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Created by TooNies1810 on 5/29/17.
 */
public class StringCalculatorTest extends TestCase {

    private StringCalculator stringCalculator = new StringCalculator();


    public void testReturnZeroIfEmptyString() throws Exception {
        assertEquals(0, stringCalculator.add(""));
    }

    public void testReturnValueIfHasOneElement() throws Exception {
        assertEquals(2, stringCalculator.add("2"));
    }

    public void testReturnSumResultIfHasTwoElement() throws Exception {
        assertEquals(5, stringCalculator.add("2,3"));
    }

    public void testReturnSumResultIfHasFiveElement() throws Exception {
        assertEquals(11, stringCalculator.add("2,3,1,2,3"));
    }

    public void testAllowNewLineBetweenNumbers() throws Exception {
        assertEquals(11, (stringCalculator.add("2\n3,1,2,3")));
    }

    public void testAllowDifferentDelimiters() throws Exception {
        assertEquals(11, stringCalculator.add("//[;]\n2;3;1;2;3"));
    }

    public void testConvertToNumberArray() {
        assertEquals("1,2,3", stringCalculator.convertToNumberArray("//[;]\n1;2;3"));
    }

    public void testWithNegativeNumber() {
        boolean error = false;

        try {
            stringCalculator.add("1,2,-2,-3");
        } catch (Exception e) {
            e.printStackTrace();
            error = true;
        }

        assertTrue(error);
    }

    public void testIgnoreNumberGreaterThan1000() throws Exception{
        assertEquals(5, stringCalculator.add("1,2,1002,2"));
    }

    public void testLengthDelimiterGreaterThan1() throws Exception{
        assertEquals(11, stringCalculator.add("//[***]\n2***3***1***2***3"));
    }

    public void testMultiDelimiter() throws Exception{
        assertEquals(11, stringCalculator.add("//[***][;][,]\n2***3;1***2,3"));
    }

}
