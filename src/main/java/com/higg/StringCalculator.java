package com.higg;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by TooNies1810 on 5/29/17.
 */
public class StringCalculator {

    public int add(String inputString) throws Exception {
        String numbers = convertToNumberArray(inputString);

        if (numbers.length() == 0) {
            return 0;
        } else {
            String elementStrings[] = numbers.split(",");

            if (elementStrings.length > 1) {
                int result = 0;
                boolean hasNegativeNumber = false;
                String negativeNumbers = "";
                for (String elementString : elementStrings) {
                    int number = Integer.parseInt(elementString);
                    if (number > 0 && number <= 1000){
                        result += number;
                    } else if(number > 1000){
                        // ignore number greater than 1000
                    } else {
                        negativeNumbers += " " + number;
                        hasNegativeNumber = true;
                    }
                }

                if (hasNegativeNumber){
                    throw new Exception(negativeNumbers);
                }

                return result;
            } else {
                return Integer.parseInt(elementStrings[0]);
            }
        }
    }

    public String convertToNumberArray(String inputString) {
        String inputRegexPattern = "//(.*)\n(.*)";
        Pattern patternInput = Pattern.compile(inputRegexPattern);
        Matcher matcherInput = patternInput.matcher(inputString);

        String numbers = inputString.trim();

        if (matcherInput.matches()) {
            String delimiterString = matcherInput.group(1).replace("[", "").replace("]", "");
//            String delimiterString = matcherInput.group(1);
//            Pattern delimiterPattern = Pattern.compile("\\[(.*?)]");
//            Matcher delimiterMatcher = delimiterPattern.matcher(delimiterString);

            numbers = matcherInput.group(2);
//            for (int i = 1; i <= delimiterMatcher.groupCount(); i++) {
//                numbers = numbers.replace(delimiterMatcher.group(i), ",");
//            }
            numbers = numbers.replace(delimiterString, ",");
        }

        numbers = numbers.replaceAll("\n", ",");

        return numbers;
    }
}
